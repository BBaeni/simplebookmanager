package ch.teko.vo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.time.Year;

import org.junit.Test;

public class BookTest {

	@Test
	public void getAuthorsAsString() {
		Book book = new Book(-1L, "", "", Year.of(2016), "");
		book.addAuthor(new Author("Muster", "Hans"));
		book.addAuthor(new Author("Muster", "Hansli"));

		String result = book.getAuthorsAsString();
		assertThat(result, is(equalTo("Muster Hans, Muster Hansli")));
	}
}
