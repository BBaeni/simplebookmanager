package ch.teko.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.junit.Test;

import ch.teko.vo.Book;

public class MySqlBookDAOTest {

	@Test
	public void getAllBooksWhenGetAllBooksThenGetListOfBooks() {
		// Arrange
		BookDAO testee = new MySqlBookDAO();
		// Act
		List<Book> result = testee.getAllBooks();
		// Assert
		assertThat(result, hasSize(greaterThan(0)));
	}
}
