package ch.teko.vo;

import java.time.Year;
import java.util.List;
import java.util.stream.Collectors;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;

public class Book {
	private final long id;
	private final SimpleStringProperty titleProperty;
	private final SimpleStringProperty descriptionProperty;
	private final SimpleIntegerProperty publishedYearProperty;
	private final SimpleStringProperty isbnProperty;
	private final ListProperty<Author> authorsProperty = new SimpleListProperty<>(FXCollections.observableArrayList());

	public Book() {
		this(-1L, "", "", Year.of(0), "");
	}

	public Book(Long bookId, String title, String description, Year publishedYear, String isbn) {
		id = bookId;
		this.titleProperty = new SimpleStringProperty(title);
		this.descriptionProperty = new SimpleStringProperty(description);
		this.publishedYearProperty = new SimpleIntegerProperty(publishedYear.getValue());
		this.isbnProperty = new SimpleStringProperty(isbn);
	}

	public long getId() {
		return id;
	}

	public String getTitle() {
		return titleProperty.get();
	}

	public int getPublishedYear() {
		return publishedYearProperty.get();
	}

	public String getIsbn() {
		return isbnProperty.get();
	}

	public List<Author> getAuthors() {
		return authorsProperty.get();
	}

	public String getDescription() {
		return descriptionProperty.get();
	}

	public void addAuthor(Author author) {
		authorsProperty.get().add(author);
	}

	public String getAuthorsAsString() {
		return authorsProperty.get().stream().map(a -> a.getLastName() + " " + a.getFirstName())
				.collect(Collectors.joining(", "));
	}

	public SimpleIntegerProperty getPublishedYearProperty() {
		return publishedYearProperty;
	}

	public SimpleStringProperty getDescriptionProperty() {
		return descriptionProperty;
	}

	public SimpleStringProperty getIsbnProperty() {
		return isbnProperty;
	}

	public SimpleStringProperty getTitleProperty() {
		return titleProperty;
	}

	public ListProperty<Author> getAuthorsProperty() {
		return authorsProperty;
	}
}
