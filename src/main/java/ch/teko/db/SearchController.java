package ch.teko.db;

import java.time.Year;

import ch.teko.vo.Book;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.util.StringConverter;

public class SearchController {

	private BookModel model;
	@FXML
	private Button searchButton;
	@FXML
	private ListView<Book> listView;
	@FXML
	private TextField titleText;
	@FXML
	private TextField authorText;
	@FXML
	private TextField yearText;
	@FXML
	private TextField isbnText;

	public SearchController(BookModel model) {
		this.model = model;
	}

	public void initialize() {
		searchButton.setOnAction(event -> {
			listView.getItems().clear();
			Year year = null;
			if (!yearText.getText().isEmpty()) {
				int intYear = Integer.parseInt(yearText.getText());
				year = Year.of(intYear);
			}
			listView.getItems()
					.addAll(model.searchBooks(titleText.getText(), authorText.getText(), year, isbnText.getText()));
		});
		listView.setCellFactory(lv -> {
			TextFieldListCell<Book> cell = new TextFieldListCell<>();
			cell.setConverter(new StringConverter<Book>() {

				@Override
				public String toString(Book book) {
					return book.getTitle();
				}

				@Override
				public Book fromString(String string) {
					return null;
				}
			});
			return cell;
		});
	}
}
