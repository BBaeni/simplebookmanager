package ch.teko.db;

import java.lang.reflect.Constructor;

import ch.teko.dao.BookDAO;
import ch.teko.dao.MySqlBookDAO;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Callback;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("main.fxml"));
		Callback<Class<?>, Object> controllerFactory = getControllerFactory();
		loader.setControllerFactory(controllerFactory);
		primaryStage.setScene(new Scene(loader.load()));
		primaryStage.setTitle("Library-Management");
		primaryStage.show();
	}

	private Callback<Class<?>, Object> getControllerFactory() {
		Callback<Class<?>, Object> controllerFactory = type -> {
			try {
				for (Constructor<?> c : type.getConstructors()) {
					if (c.getParameterCount() == 1 && c.getParameterTypes()[0] == BookModel.class) {
						return c.newInstance(buildModel());
					}
				}
				return type.newInstance();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
		return controllerFactory;
	}

	private BookModel buildModel() {
		return new BookModel(buildDAO());
	}

	private BookDAO buildDAO() {
		return new MySqlBookDAO();
	}

	public static void main(String[] args) throws Exception {
		launch(args);
	}

}
