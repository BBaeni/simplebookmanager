package ch.teko.db;

import java.time.Year;
import java.util.List;

import ch.teko.dao.BookDAO;
import ch.teko.vo.Book;

public class BookModel {

	private BookDAO bookDAO;

	public BookModel(BookDAO buildDAO) {
		this.bookDAO = buildDAO;
	}

	public List<Book> getAllBooks() {
		return bookDAO.getAllBooks();
	}

	public void addNewBook(Book book) {
		bookDAO.addBook(book);
	}

	public void updateBook(Book book) {
		bookDAO.updateBook(book);
	}

	public List<Book> searchBooks(String title, String author, Year year, String isbn) {
		return bookDAO.getBooksByProperty(title, author, year, isbn);
	}
}
