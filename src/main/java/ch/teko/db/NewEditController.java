package ch.teko.db;

import ch.teko.vo.Author;
import ch.teko.vo.Book;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.util.converter.NumberStringConverter;

public class NewEditController {

	private Book book;
	@FXML
	private TextField titleText;
	@FXML
	private TextField erscheinugsjahrText;
	@FXML
	private TableView<Author> authorTableView;
	@FXML
	private TextField isbnText;
	@FXML
	private TextArea descriptionText;
	@FXML
	private Button neuButton;
	@FXML
	private Button deleteButton;
	@FXML
	private TableColumn<Author, String> lastNameColumn;
	@FXML
	private TableColumn<Author, String> firstNameColumn;

	public NewEditController(Book book) {
		this.book = book;
	}

	public void initialize() {
		initTableView();

		titleText.textProperty().bindBidirectional(book.getTitleProperty());
		erscheinugsjahrText.textProperty().bindBidirectional(book.getPublishedYearProperty(),
				new NumberStringConverter());
		authorTableView.itemsProperty().bindBidirectional(book.getAuthorsProperty());
		isbnText.textProperty().bindBidirectional(book.getIsbnProperty());
		descriptionText.textProperty().bindBidirectional(book.getDescriptionProperty());

		neuButton.setOnAction(event -> {
			Dialog<Author> newDialog = new Dialog<>();
			Label label1 = new Label("Nachname ");
			Label label2 = new Label("Vorname ");
			TextField lastnameText = new TextField();
			TextField firstnameText = new TextField();

			GridPane grid = new GridPane();
			grid.add(label1, 1, 1);
			grid.add(lastnameText, 2, 1);
			grid.add(label2, 1, 2);
			grid.add(firstnameText, 2, 2);
			newDialog.getDialogPane().setContent(grid);

			ButtonType buttonTypeOk = new ButtonType("OK", ButtonData.OK_DONE);
			newDialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
			newDialog.showAndWait();
			authorTableView.getItems().add(new Author(lastnameText.getText(), firstnameText.getText()));
		});

		deleteButton.setOnAction(event -> {
			authorTableView.getItems().remove(authorTableView.getSelectionModel().getSelectedItem());
		});
	}

	private void initTableView() {
		lastNameColumn.setCellValueFactory(author -> new SimpleStringProperty(author.getValue().getLastName()));
		firstNameColumn.setCellValueFactory(author -> new SimpleStringProperty(author.getValue().getFirstName()));
	}

}
