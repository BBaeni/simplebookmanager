package ch.teko.db;

import java.util.Optional;

import ch.teko.vo.Book;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;

public class AdministrationController {

	@FXML
	private TableView<Book> tableView;
	@FXML
	private TableColumn<Book, String> titleColumn;
	@FXML
	private TableColumn<Book, String> authorColumn;
	@FXML
	private TableColumn<Book, Number> publishedYearColumn;
	@FXML
	private TableColumn<Book, String> isbnColumn;
	@FXML
	private Button newButton;
	@FXML
	private Button editButton;
	@FXML
	private Button deleteButton;

	private BookModel model;

	public AdministrationController(BookModel model) {
		this.model = model;
	}

	public void initialize() {
		createBookTableColumns();
		loadAllBooks();
		newButton.setOnAction((event) -> {
			showDialog(new Book());
		});
		editButton.setOnAction(event -> {
			showDialog(tableView.getSelectionModel().getSelectedItem());
		});
		tableView.setRowFactory(r -> {
			TableRow<Book> row = new TableRow<>();
			row.setOnMouseClicked(e -> {
				if (e.getClickCount() == 2 && !row.isEmpty()) {
					showDialog(row.getItem());
				}
			});
			return row;
		});
		tableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				editButton.setDisable(false);
				deleteButton.setDisable(false);
			} else {
				editButton.setDisable(true);
				deleteButton.setDisable(true);
			}
		});
	}

	private void loadAllBooks() {
		tableView.getItems().clear();
		tableView.getItems().addAll(model.getAllBooks());
	}

	private void createBookTableColumns() {
		titleColumn.setCellValueFactory(book -> new SimpleStringProperty(book.getValue().getTitle()));
		authorColumn.setCellValueFactory(book -> new SimpleStringProperty(book.getValue().getAuthorsAsString()));
		publishedYearColumn.setCellValueFactory(book -> new SimpleIntegerProperty(book.getValue().getPublishedYear()));
		isbnColumn.setCellValueFactory(book -> new SimpleStringProperty(book.getValue().getIsbn()));
	}

	private void showDialog(final Book book) {
		Dialog<ButtonType> dialog = new Dialog<>();
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("newEditDialog.fxml"));
		loader.setControllerFactory(t -> new NewEditController(book));
		try {
			dialog.getDialogPane().setContent(loader.load());
		} catch (Exception e) {
			e.printStackTrace();
		}
		dialog.getDialogPane().getButtonTypes().addAll(ButtonType.APPLY, ButtonType.CLOSE);
		Node closeButton = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
		closeButton.managedProperty().bind(closeButton.visibleProperty());
		Optional<ButtonType> result = dialog.showAndWait();
		if (result.get() == ButtonType.APPLY) {
			if (book.getId() == -1L) {
				model.addNewBook(book);
			} else {
				model.updateBook(book);
			}
			loadAllBooks();
		}
	}

}
