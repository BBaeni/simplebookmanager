package ch.teko.dao;

import java.time.Year;
import java.util.List;

import ch.teko.vo.Book;

public interface BookDAO {
	public List<Book> getAllBooks();

	public void addBook(Book book);

	public void updateBook(Book book);

	public void deleteBook(Book book);

	public List<Book> getBooksByProperty(String title, String author, //
			Year publishedYear, String isbn);
}
