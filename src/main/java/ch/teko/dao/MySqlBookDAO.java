package ch.teko.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Year;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.teko.vo.Author;
import ch.teko.vo.Book;

public class MySqlBookDAO implements BookDAO {

	@Override
	public List<Book> getAllBooks() {
		List<Book> results = new ArrayList<>();
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(
						"select book_id, title, description, publishedyear, isbn_nr, lastname, firstname "
								+ "from book join book_author using(book_id) join author using(author_id) "
								+ "join publishedyear using(publishedyear_id) join isbn using(book_id)");
				ResultSet rs = statement.executeQuery();) {
			buildBookListFromResultSet(results, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return results;
	}

	private void buildBookListFromResultSet(List<Book> results, ResultSet rs) throws SQLException {
		Map<Long, Book> booksById = new HashMap<>();
		while (rs.next()) {
			Long bookId = rs.getLong(1);
			String title = rs.getString(2);
			String description = rs.getString(3);
			int publishedYear = rs.getInt(4);
			String isbn = rs.getString(5);
			Book book = booksById.get(bookId);
			if (book == null) {
				book = new Book(bookId, title, description, Year.of(publishedYear), isbn);
				booksById.put(bookId, book);
			}
			book.addAuthor(new Author(rs.getString(6), rs.getString(7)));
		}
		results.addAll(booksById.values());
	}

	@Override
	public void updateBook(Book book) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteBook(Book book) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addBook(Book book) {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(
						"insert into book (title, publishedyear_id, description) values (?, ?, ?);",
						Statement.RETURN_GENERATED_KEYS);) {
			statement.setString(1, book.getTitle());
			statement.setInt(2, getPublishedYearId(book.getPublishedYear()));
			statement.setBytes(3, book.getDescription().getBytes());
			statement.executeUpdate();
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
				int bookId = rs.getInt(1);
				insertIsbn(bookId, book.getIsbn());
				insertBookAuthors(bookId, book.getAuthors());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private int getPublishedYearId(int publishedYear) throws SQLException {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn
						.prepareStatement("select publishedyear_id from publishedyear where publishedyear = ?;");) {
			statement.setInt(1, publishedYear);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				return rs.getInt(1);
			}
		}
		return insertNewPublishedYear(publishedYear);
	}

	private int insertNewPublishedYear(int publishedYear) throws SQLException {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(
						"insert into publishedyear (publishedyear) values (?);", Statement.RETURN_GENERATED_KEYS);) {
			statement.setInt(1, publishedYear);
			statement.executeUpdate();
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
				return rs.getInt(1);
			}
			return -1;
		}
	}

	private void insertIsbn(int bookId, String isbn) throws SQLException {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement("insert into isbn values (?, ?);");) {
			statement.setInt(1, bookId);
			statement.setString(2, isbn);
			statement.executeUpdate();
		}
	}

	private void insertBookAuthors(int bookId, List<Author> authors) throws SQLException {
		for (int authorId : getAuthorIds(authors)) {
			try (Connection conn = ConnectionManager.getConnection();
					PreparedStatement statement = conn
							.prepareStatement("insert into book_author (book_id, author_id) values (?, ?);");) {
				statement.setInt(1, bookId);
				statement.setInt(2, authorId);
				statement.executeUpdate();
			}
		}
	}

	private List<Integer> getAuthorIds(List<Author> authors) throws SQLException {
		List<Integer> authorIds = new ArrayList<>();
		for (Author author : authors) {
			try (Connection conn = ConnectionManager.getConnection();
					PreparedStatement statement = conn
							.prepareStatement("select author_id from author where firstname = ? and lastname = ?;");) {
				statement.setString(1, author.getFirstName());
				statement.setString(2, author.getLastName());
				ResultSet rs = statement.executeQuery();
				if (rs.next()) {
					authorIds.add(rs.getInt(1));
				} else {
					authorIds.add(insertNewAuthor(author));
				}
			}
		}
		return authorIds;
	}

	private int insertNewAuthor(Author author) throws SQLException {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(
						"insert into author (firstname, lastname) values (?, ?);", Statement.RETURN_GENERATED_KEYS);) {
			statement.setString(1, author.getFirstName());
			statement.setString(2, author.getLastName());
			statement.executeUpdate();
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
				return rs.getInt(1);
			}
		}
		return -1;
	}

	@Override
	public List<Book> getBooksByProperty(String title, String author, Year publishedYear, String isbn) {
		List<Book> results = new ArrayList<>();
		String query = "select book_id, title, description, publishedyear, isbn_nr, lastname, firstname "
				+ "from book join book_author using(book_id) join author using(author_id) "
				+ "join publishedyear using(publishedyear_id) join isbn using(book_id) where 1=1";
		if (title != null) {
			query += " and title like '%" + title + "%'";
		}
		if (author != null) {
			query += " and (lastname like '%" + author + "%' or firstname like '%" + author + "%')";
		}
		if (publishedYear != null) {
			query += " and publishedyear = " + publishedYear.getValue();
		}
		if (isbn != null) {
			query += " and isbn_nr like '%" + isbn + "%'";
		}
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement statement = conn.prepareStatement(query);
				ResultSet rs = statement.executeQuery();) {
			buildBookListFromResultSet(results, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return results;
	}

}
