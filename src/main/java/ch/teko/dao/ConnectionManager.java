package ch.teko.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class ConnectionManager {
	private static ConnectionManager instance = new ConnectionManager();
	private static final String DB_URL = "jdbc:mysql://localhost:3306/books";
	private static final String USER = "TestUser";
	private static final String PASS = "TestUser";

	private ConnectionManager() {
	}

	private Connection createConnection() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static Connection getConnection() {
		return instance.createConnection();
	}
}
